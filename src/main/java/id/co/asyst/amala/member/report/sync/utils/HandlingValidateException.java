package id.co.asyst.amala.member.report.sync.utils;

public class HandlingValidateException extends Exception {

    public HandlingValidateException(String message) {
        super(message);
    }
}
