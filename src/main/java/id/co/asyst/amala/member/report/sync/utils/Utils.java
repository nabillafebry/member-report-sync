package id.co.asyst.amala.member.report.sync.utils;

import org.apache.camel.Exchange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

public class Utils {
    private static Logger log = LogManager.getLogger(Utils.class);

    public void getDetails(Exchange exchange) {
        Map<String, Object> member = (Map<String, Object>) exchange.getProperty("resultResponseMemberProfile");
        List<Map<String, Object>> addresses = (List<Map<String, Object>>) member.get("memberaddress");
        List<Map<String, Object>> contacts = (List<Map<String, Object>>) member.get("membercontacts");
        List<Map<String, Object>> account = (List<Map<String, Object>>) member.get("memberaccount");
        Map<String, Object> membertier = (Map<String, Object>) account.get(0).get("membertier");
        String address = null;
        String phone = null;
        String city = null;
        String country = null;
        double awardmiles = 0;
        double frequency = 0;
        double tiermiles = 0;
        String tier = (String) membertier.get("tiername");

        for (Map<String, Object> memberaddress : addresses) {
            if (memberaddress.get("ispreffered").equals(true)) {
                address = (String) memberaddress.get("address");
                city = (String) memberaddress.get("cityname");
                country = (String) memberaddress.get("countryname");
            }
        }

        for (Map<String, Object> contact : contacts) {
            if (contact.get("preferrednumber").equals(true)) {
                phone = (String) contact.get("phonenumber");
            }
        }

        for (Map<String, Object> acc : account) {
            awardmiles = (double) acc.get("awardmiles");
            frequency = (double) acc.get("frequency");
            tiermiles = (double) acc.get("tiermiles");
        }


        DecimalFormat df = new DecimalFormat("###.#");

        exchange.setProperty("address", address);
        exchange.setProperty("city", city);
        exchange.setProperty("country", country);
        exchange.setProperty("phone", phone);
        exchange.setProperty("awardmiles", df.format(awardmiles));
        exchange.setProperty("frequency", df.format(frequency));
        exchange.setProperty("tiermiles", df.format(tiermiles));
        exchange.setProperty("tier", tier);
    }


}
